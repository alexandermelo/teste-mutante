
Desenvolvi o app em Node utilizando `express` para o backend e o clássico `angularjs` para o front (com o `bootstrap`). As requisições foram feitas com o framework `request`, que converte a requisição em um promise.

# Variáveis de ambiente

A aplicacão precisa de DUAS variáveis do ambiente, sendo elas o `PORT` (porta HTTP utilizada) e o `URL_ES` (endereço do ElasticSearch), elas possuem valor padrão no Dockerfile, mas na aplicacão não.

# E o ElasticSearch?

É possível subir uma instância gratuitamente, mas é possível rodar ela pelo Docker com os dois comandos abaixo:
```
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.2.0
docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.2.0
```
Com isso o nosso ElasticSearch vai rodar na versão `7.2.0` e no endereço `http://localhost:9200/`.

# Docker, como faço?

Primeiro: tenha o Docker instalado. Segundo: Rode o comando abaixo:
```
docker build -t desafio-mutant --build-arg PORTA_WEB_APP=8080 --build-arg URL_ELASTIC_SEARCH=localhost:9200 .
```
E, por último, o comando abaixo:
```
docker run -p 80:8080 desafio-mutant
```
Após a mensagem que a aplicação iniciou, abra o seu navegador favorito e entre em `http://localhost` (sem porta mesmo).


