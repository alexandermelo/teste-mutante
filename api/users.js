
const esClient = require('../elastic-search-client')

//URL do desafio, já testei o retorno no browser
//Exemplo de retorno JSON da API
/* [
  {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
 ] */
const url = 'https://jsonplaceholder.typicode.com/users'

//Os websites de todos os usuários
const getWebsites = (users) => users.map(user => user.website)

// O Nome, email e a empresa em que trabalha (em ordem alfabética)
const getUsuariosOrdenadosNome = (users) =>
  users.sort((x, y) => x.name > y.name ? 1 : 0).map(user => {
    let sortReturn = {
      'nome': user.name,
      'email': user.email,
      'empresa': user.company.name
    }
    return sortReturn
  })

//Mostrar todos os usuários que no endereço contem a palavra ```suite```
const getUsuariosMorandoEmSuite = (users) => users
  .filter(u => u.address.suite.toLowerCase().includes('suite'))
  .map(u => u.username)

module.exports = {
  Rest: require('express')()
    .get('/', (req, res) => {
      require('request')(url, { json: true }, (err, response, usuarios) => {

        if (err) res.status(500).send(err.message)
        const jsonResposta = {
          websites: getWebsites(usuarios),
          nomeEmailEmpresa: getUsuariosOrdenadosNome(usuarios),
          usuariosMorandoEmSuite: getUsuariosMorandoEmSuite(usuarios)
        }
        esClient.salvarConsultar(jsonResposta)
        res.send(jsonResposta)

      })
    })
    .get('/teste', (req, res) => res.json('App funcionando!'))
}